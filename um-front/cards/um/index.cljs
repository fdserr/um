(ns um.index
  (:require
   [devcards.core
    :refer [defcard defcard-rg start-devcard-ui!]
    :include-macros true]
   [um.core :as um]
   [um.ui :as ui]
   [datascript.core :as d]
   [reagent.core :as r]
   [taoensso.timbre :as timbre]))

(when-let [e (.getElementById js/document "app-cards")]
  (.removeChild (.-parentNode e) e))

(start-devcard-ui!)

; (defcard-rg card
;   "UI APP"
;   (fn [*s _]
;     [um/ui-app *s])
;   (um/start-app! (r/atom {})))

(defcard-rg card-users
  "Users"
  (fn [*s _]
    [ui/users @*s])
  {:datasource
   (r/atom
    [{:db/id 1 :user/email "1a@b.c"}
     {:db/id 2 :user/email "2a@b.c"}])})

(defcard-rg card-new-user-form
  "New User Form"
  (fn [*s _]
    [ui/new-user-form @*s])
  (um/start-app!
   (r/atom {})
   {:user/email {:db/unique :db.unique/value}}
   [{:db/id "1" :user/email "1a@b.c"}
    {:db/id "2" :user/email "2a@b.c"}]))

(defcard-rg card-input-text
  "Input Text"
  (fn [*s _]
    [ui/input-text 
     {:id :user/email
      :value @*s
      :on-change #(reset! *s (-> % .-target .-value))}])
  (r/atom "Input value"))
