(ns um.main
  (:require
   [reagent.core :as r]
   [um.core :as um]
   [um.ui :as ui]))

(defonce *app-store (um/start-app! (r/atom {}) {} []))
(um/render-app! *app-store ui/app "app")
