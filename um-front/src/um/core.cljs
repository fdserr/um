(ns um.core
  (:require
   [cljs.reader :as reader]
   [datascript.core :as d]
   [goog.string :as gstring]
   [goog.string.format]
   [reagent.core :as r]
   [taoensso.timbre :as timbre]))

(def ^:dynamic *dispatch-fn* nil)
(def ^:dynamic **store* nil)

(defn transact
  ([state tx-data]
   (transact state tx-data nil))
  ([state tx-data tx-meta]
   (let [tx-report (d/with (:__db state) tx-data tx-meta)]
     (timbre/debug "[TRANSACT] report: " tx-report)
     (assoc state
            :__last-tx (:tx-data tx-report)
            :__db (:db-after tx-report)))))

(defn init-store!
  [*store schema tx]
  (reset!
   *store
   {:__db (d/empty-db schema)
    :__last-tx []})
  (swap! *store transact tx)
  (timbre/debug "[STORE] initialised")
  nil)

(defmulti transition
  (fn [action & _] action))

(defn- disp!
  [*store action & args]
  {:pre [#(satisfies? ISwap %1)]}
  (timbre/debug "[DISPATCH] action: " action)
  (swap! *store #(apply transition action % args))
  nil)

(defn mk-dispatch-fn
  [*store]
  (partial disp! *store))

;;;

; (defmethod transition :inc
;   [action app-state k]
;   (timbre/debug "[TRANSITION]" action k)
;   (transact app-state [[:key k] :count 0]))
;
; (defn ui-btn
;   [dispatch! text k]
;   (timbre/debug "[REAGENT] rendering" k)
;   [:button
;    {:on-click #(dispatch! :inc k)}
;    text])
;
; ;;;
;
; (defmethod transition :change-tx-text
;   [action app-state text]
;   (timbre/info "[TRANSITION]" action text)
;   (assoc app-state :tx-text text))
;
; (defmethod transition :run-tx
;   [action app-state]
;   (timbre/info "[TRANSITION]" action)
;   (try
;     (transact app-state (reader/read-string (:tx-text app-state)))
;     (catch js/Error e
;       (timbre/error "[TRANSITION]" action "failed:" (.-message e))
;       (assoc app-state :tx-error (.-message e)))))
;
; (defn ui-tx
;   [{:keys [dispatch! text error db last-tx]}]
;   [:div
;    [:div "TX:"
;     [:pre (str last-tx)]
;     [:br] [:span error]]
;    [:textarea
;     {:value text
;      :on-change #(dispatch! :change-tx-text (-> % (.-target) (.-value)))}]
;    [:br] [:button
;           {:on-click #(dispatch! :run-tx)}
;           "Transact"]])

;;;

; (defn fetch
;   [*store k]
;   (timbre/info "[FETCH] data for " k)
;   (let [item (k @*store)
;         {:keys [text count]} item]
;     (str text " : " count)))

;;;

(defn fetch-last-tx
  [*store]
  (:__last-tx @*store))

(defn fetch-db
  [*store]
  (:__db @*store))

(defn fetch-xs
  [*store k]
  (timbre/debug "[FETCH] xs" k)
  (d/q '[:find [(pull ?e [*]) ...]
         :in $ ?k
         :where [?e ?k _]]
       (:__db @*store) k))

(defmethod transition :tx
  [_ state tx]
  (transact state tx))

(defn ui-list
  [{:keys [datasource on-click label]}]
  (timbre/debug "[UI LIST] rendering" label)
  [:div
   [:ul label
    (for [x @datasource]
      ^{:key (:db/id x)} [:li (str x)])]
   [:button {:on-click on-click} (gstring/format "%s!" label)]])

(defn ui-txr
  [{:keys [datasource]}]
  [:div "TXR:" (str (mapv vec @datasource))])

(defn ui-app
  [*store]
  (r/with-let [*txr (r/track fetch-last-tx *store)
               *xs (r/track fetch-xs *store :x)
               *ys (r/track fetch-xs *store :y)
               dispatch! (mk-dispatch-fn *store)
               click-x #(dispatch! :tx [{:x 1}])
               click-y #(dispatch! :tx [{:y 1}])]
    (timbre/debug "[UI APP] rendering")
    [:div "APP"
     [ui-txr {:datasource *txr}]
     [ui-list {:datasource *xs :on-click click-x :label "X"}]
     [ui-list {:datasource *ys :on-click click-y :label "Y"}]]))

(defn render-app!
  [*store ui elem-id]
  (if-let [node (.getElementById js/document elem-id)]
    (r/render [ui *store] node)
    (timbre/warn "[RENDER] App root site not found:" elem-id)))


(defn start-app!
  [*store schema tx]
  (init-store! *store schema tx)
  (timbre/info "[APP] App started.")
  *store)
