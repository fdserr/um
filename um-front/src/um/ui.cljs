(ns um.ui
  (:require
   [um.core :as um]
   [cljs.reader :as reader]
   [datascript.core :as d]
   [goog.string :as gstring]
   [goog.string.format]
   [reagent.core :as r]
   [taoensso.timbre :as timbre]))

(defn users
  [{:keys [datasource]}]
  [:table
   [:tbody
    (for [{:keys [db/id user/email]} @datasource]
      ^{:key id}
      [:tr [:td email]])]])


(defn input-text
  [{:keys [id value error on-change]}]
  [:div
   [:label {:for (str id)} (str id)]
   [:input
    {:id (str id)
     :type "text"
     :value value
     :on-change on-change}]
   [:span error]])

(defn fetch-user
  [*store email]
  (timbre/debug "[FETCH] user" email)
  (let [db (:__db @*store)]
    (d/touch (d/entity db [:user/email email]))))

(defmulti input-changed
  (fn [id state *change-set old-val new-val] id)
  :default :input-changed/default)

(defmethod input-changed :input-changed/default
  [id state *change-set old-val new-val]
  (timbre/debug "[INPUT] *change-set" *change-set)
  (swap! *change-set #(-> % (assoc id {:old old-val :new new-val})))
  (timbre/debug "[INPUT] changed" id)
  state)

(defmethod um/transition :input-changed
  [k state *change-set id old-val new-val]
  (timbre/debug "[TRANSITION]" k @*change-set)
  (input-changed id state *change-set old-val new-val))

; (defn fetch-change-set
;   [*store form-id]
;   (let [db (:__db @*store)]))


(defn new-user-form
  [*store]
  (r/with-let [dispatch! (um/mk-dispatch-fn *store)
               *change-set (r/atom {})
               fetch-user-email (fn [*change-set]
                                  (:new (:user/email @*change-set)))
               *email (r/track fetch-user-email *change-set)
               fetch-user-email-error (fn [*change-set]
                                        (:error (:user/email @*change-set)))
               *email-error (r/track fetch-user-email-error *change-set)]
    (let []
      [:form @*email
       [input-text
        {:id :user/email
         :value @*email
         :error @*email-error
         :on-change #(dispatch! :input-changed *change-set
                                :user/email @*email (-> % .-target .-value))}]])))


(defn app
  [*store]
  [:div "APP"])
