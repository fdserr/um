(ns um.core-tests
  (:require
   [cljs-test-display.core :refer [init!]]
   [cljs.test :refer-macros [deftest testing is run-tests async]]))

(deftest should-not-pass
  (is (= 20 20)))

(run-tests
 (init! "app-tests"))
