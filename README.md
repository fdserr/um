# um

clj/s scaffolded

### Start clj REPL

clj -Aum-back:repl

### Start FW REPL (new terminal)

clj -Aum-front:fw


### App at:

http://localhost:9500/

### Tests at:

http://localhost:9500/figwheel-extra-main/tests

### Devcards at:

http://localhost:9500/figwheel-extra-main/cards
